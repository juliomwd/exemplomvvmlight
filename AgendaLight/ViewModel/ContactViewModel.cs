using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using AgendaLight.Model;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace AgendaLight.ViewModel
{


    public class ContactViewModel : ViewModelBase
    {

        private ObservableCollection<ObservableObject> _contacts;

        public ObservableCollection<ObservableObject> Contacts
        {
            get { return _contacts; }
            set
            {
                Set(ref _contacts, value);
            }
        }

        public ContactViewModel()
        {
            this.Contacts = new ObservableCollection<ObservableObject>();
        }

        public ICommand MyCommand => new RelayCommand<ObservableCollection<ObservableObject>>(
            (ObservableCollection<ObservableObject> contacts) =>
            {
                this.Contacts.Add(new User()
                {
                    Id = 1,
                    Email = "jc@mail.com",
                    Name = "Julio C�sar",
                    Phone = "1242424"
                }
                );
            },
            (ObservableCollection<ObservableObject> contacts) =>
            {

                if (contacts != null && contacts.Count > 1)
                {
                    return false;
                }
                return true;
            }
        );
    }
}
