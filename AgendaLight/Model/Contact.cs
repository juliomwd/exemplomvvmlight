﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgendaLight.Model
{
    class Contact : ObservableObject
    {
        public Contact()
        {
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { Set(ref id, value); }
        }


        private string name;

        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { Set( ref email, value); }
        }

        private string phone;

        public string Phone
        {
            get { return phone; }
            set { Set(ref phone, value); }
        }



    }
}
